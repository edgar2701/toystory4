﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using ToyStory4.Service;

namespace ToyStory4.Controllers
{
    public class pt_PTController : Controller
    {

        public JsonResult GetId(string id)
        {                       
            CodeService _codeService = new CodeService();
            var _result = false;
            if (!string.IsNullOrEmpty(Session["codigo_cod"] as string) &&
                !string.IsNullOrEmpty(((int)Session["id_cod"]).ToString() as string))
            {
                _result = _codeService.UpdateValidatedCode((string)Session["codigo_cod"], (int)Session["id_cod"], id);
            }       
            var data = "";
            if (_result)
            {
                data = "{ \"codigo_cod\" : \"" + (string)Session["codigo_cod"] + " \" , \"id_cod\" : \"" + ((int)Session["id_cod"]).ToString() + "\" , \"IdKellogs\" : \"" + id + "\" }";
            }
            else {
                data = "{ \"codigo_cod\" : \"error\"}";
            }
            return Json(data, JsonRequestBehavior.AllowGet); 
        }


        DateTime _fechaPromo = Convert.ToDateTime(WebConfigurationManager.AppSettings["FechaPromo"]);
        public ActionResult Index()
        {
            if (_fechaPromo <= DateTime.Now)
                return View("~/Views/pt_PT/Code/DateInvalid.cshtml");
            return View();
        }

        public ActionResult Congrats()
        {

            return View();
        }
        public ActionResult Sorry()
        {

            return View();
        }

        public ActionResult Login()
        {

            return View();
        }

        public ActionResult Register()
        {

            return View();
        }

        public ActionResult ValidateCode(string codigo_cod)
        {
            CodeService _codeService = new CodeService();
            var _result = _codeService.ValidateCode(codigo_cod);
            
            if (_fechaPromo <= DateTime.Now)
                return View("~/Views/pt_PT/Code/DateInvalid.cshtml");

            if (_result.Codigo_cod != null)
            {
                Session["codigo_cod"] = codigo_cod;
                Session["id_cod"] = _result.Id_cod;
                return View("~/Views/pt_PT/Code/Valid.cshtml");
              
            }        
                return View("~/Views/pt_PT/Code/Invalid.cshtml");                                    
        }

        

    }
}