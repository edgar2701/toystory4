﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyStory4.Service
{
    public interface ICodeService
    {
         bool ValidateCode(string Code);
    }
}
