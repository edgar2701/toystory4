﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToyStory4.Model;

namespace ToyStory4.Service
{
    public class CodeService 
    {
        KellogsApp_dbEntities model = new KellogsApp_dbEntities();
        public Codigo ValidateCode(string Code)
        {
            Codigo _codigoBBDD = new Codigo();
            try
            {
                
                var _result = model.Codigos_cod.Where(c => c.codigo_cod.ToLower() == Code.ToLower() && c.estado_cod == 1).FirstOrDefault();

                if (_result != null)
                {
                    _codigoBBDD = MapCodigoEntityToClass(_result);
                }
                return _codigoBBDD;
            }
            catch(Exception e)
            {
                return _codigoBBDD;
            }
               
        }

        public bool UpdateValidatedCode(string Code, long Id, string IdKellogs)
        {
            try
            {
                var _result = (from a in model.Codigos_cod where a.id_cod == Id && a.codigo_cod == Code select a).FirstOrDefault();

                if (_result != null)
                {
                    _result.fecha_cod = DateTime.Now;
                    _result.estado_cod = 2;
                    _result.idkellogs = IdKellogs;
                    model.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        private Codigo MapCodigoEntityToClass(Codigos_cod codigoEntity)
        {
            return new Codigo()
            {
                Id_cod = codigoEntity.id_cod,
                Codigo_cod = codigoEntity.codigo_cod,
                Estado_cod = codigoEntity.estado_cod,
                Fecha_cod = codigoEntity.fecha_cod,
                IdKellogs = codigoEntity.idkellogs
            };
        }
    }
}