﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ToyStory4
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "es_ES",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "es_ES", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "pt_PT",                                              // Route name
                "{controller}/{action}/{id}",                           // URL with parameters
                new { controller = "pt_PT", action = "Index", id = "" }  // Parameter defaults
            );
        }
    }
}
